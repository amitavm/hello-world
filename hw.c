#include <stdio.h>

// Greet a person by his/her name.
void greet(const chat *name)
{
    printf("Hello %s, how are you today?\n", name);
}

// Program to greet a person by name.
int main()
{
    greet("Naren");
    return 0;
}
